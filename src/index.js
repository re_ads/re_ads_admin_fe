import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/styles.css';
import './assets/css/custom.css';
import Main from './main';
import store from './app/store';
import { Provider } from 'react-redux';
import reportWebVitals from './reportWebVitals';

import './i18n';

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <Main />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
