import { useState } from 'react';

export function useError(
  initState = {
    error: false,
    message: '',
  }
) {
  const [error, regularSetError] = useState(initState);

  const setError = (newError) => {
    regularSetError((prevError) => ({
      ...prevError,
      ...newError,
    }));
  };

  return [error, setError];
}
