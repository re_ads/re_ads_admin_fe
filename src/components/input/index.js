import React, { useEffect, useState } from 'react';

function Input({ type, value, onChange, placeholder, error, className }) {
  const [valuePrivate, setValuePrivate] = useState('');

  useEffect(() => {
    setValuePrivate(value);
  }, [value]);

  const onChangeInput = (evt) => {
    const value = evt.target.value;
    setValuePrivate(value);
    if (typeof onChange === 'function') {
      onChange(evt);
    }
  };

  let classes = ['form-control'];
  if (className) {
    classes.push(className);
  }

  return (
    <div className='form-control-wrap'>
      <input
        type={type ? type : 'text'}
        className={classes.join(' ')}
        required=''
        placeholder={placeholder ? placeholder : ''}
        value={valuePrivate}
        onChange={onChangeInput}
      />
      <span className='invalid'>{error}</span>
    </div>
  );
}

export default Input;
