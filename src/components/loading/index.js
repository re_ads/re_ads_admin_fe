import React from 'react';
import './index.css';

function Loading() {
  return (
    <div className='loading-container'>
      <div className='loader'>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  );
}

export default Loading;
