import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import { useTranslation } from 'react-i18next';
import ButtonIcon from '../button_icon';

function ModalError({ show, message, header, subMessage, onClose }) {
  const [showPrivate, setShowPrivate] = useState(false);
  const { t } = useTranslation();

  useEffect(() => {
    if (show !== undefined) {
      setShowPrivate(show);
    }
  }, [show]);

  const onCloseModalError = () => {
    setShowPrivate(false);
    if (typeof onClose === 'function') {
      onClose();
    }
  };

  return (
    <div>
      {showPrivate &&
        ReactDOM.createPortal(
          <React.Fragment>
            <div
              className='modal fade d-block show'
              tabIndex='-1'
              aria-modal='true'>
              <div className='modal-dialog modal-sm' role='document'>
                <div className='modal-content'>
                  <div className='modal-body text-center'>
                    <div className='nk-modal'>
                      <em className='icon ni ni-alert font-size-60px text-danger'></em>
                      <h4 className='nk-modal-title mt-0 mb-2'>
                        {header ? header : t('modal_error_header')}
                      </h4>
                      <div className='nk-modal-text'>
                        <p className='lead'>{message}</p>
                        <p className='text-soft'>{subMessage}</p>
                      </div>

                      <div className='nk-modal-action'>
                        <ButtonIcon
                          className='btn btn-outline-light bg-white d-sm-inline-flex'
                          label={t('modal_error_btn_ok')}
                          onClick={onCloseModalError}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='modal-backdrop fade show'></div>
          </React.Fragment>,
          document.body
        )}
    </div>
  );
}

export default ModalError;
