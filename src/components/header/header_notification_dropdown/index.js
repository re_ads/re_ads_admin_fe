import React from 'react';

function HeaderNotificationDropdown(props) {
  return (
    <div
      className='dropdown-menu dropdown-menu-xl dropdown-menu-right dropdown-menu-s1 show position-relative'
      x-placement='bottom-end'>
      <div className='dropdown-head'>
        <span className='sub-title nk-dropdown-title'>Thông báo</span>
        <a href='#'>Đánh dấu đã đọc tất cả</a>
      </div>
      <div className='dropdown-body'>
        <div className='nk-notification'>
          <div className='nk-notification-item dropdown-inner'>
            <div className='nk-notification-icon'>
              <em className='icon icon-circle bg-warning-dim ni ni-curve-down-right'></em>
            </div>
            <div className='nk-notification-content'>
              <div className='nk-notification-text'>
                You have requested to <span>Widthdrawl</span>
              </div>
              <div className='nk-notification-time'>2 hrs ago</div>
            </div>
          </div>
          <div className='nk-notification-item dropdown-inner'>
            <div className='nk-notification-icon'>
              <em className='icon icon-circle bg-success-dim ni ni-curve-down-left'></em>
            </div>
            <div className='nk-notification-content'>
              <div className='nk-notification-text'>
                Your <span>Deposit Order</span> is placed
              </div>
              <div className='nk-notification-time'>2 hrs ago</div>
            </div>
          </div>
        </div>
      </div>
      <div className='dropdown-foot center'>
        <a href='#'>Xem tất cả</a>
      </div>
    </div>
  );
}

export default HeaderNotificationDropdown;
