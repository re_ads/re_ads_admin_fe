import React, { Fragment, useEffect, useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import { detectingDevice } from '../../utils/detectingDevice';
import { elementUtils } from '../../utils/elementUtils';

const styleCenter = {
  position: 'absolute',
  left: '50%',
  transform: 'translate(-50%, 0%)',
};

const MARGIN_TOP = 6;

function ModalDropdown({ target, modalBody, children, mobileCenter }) {
  const [transform, setTransform] = useState({ top: 0, right: 0 });
  const [show, setShow] = useState(false);

  let targetRef = useRef();
  let modalBodyRef = useRef();

  useEffect(() => {
    document.addEventListener('click', onClickOutSideModalBody);
    return () => {
      document.removeEventListener('click', onClickOutSideModalBody);
    };
  });

  const onClickOutSideModalBody = (evt) => {
    if (modalBodyRef instanceof HTMLElement) {
      const isClickInsideModal = elementUtils.isDescendantElement(
        modalBodyRef,
        evt.target
      );
      const isClickTarget = elementUtils.isDescendantElement(
        targetRef,
        evt.target
      );
      if (!isClickInsideModal && !isClickTarget) {
        setShow((prevShow) => (prevShow = false));
      }
    }
  };

  const onToggleTarget = () => {
    const boundingTarget = targetRef.getBoundingClientRect();

    const top = boundingTarget.bottom + MARGIN_TOP + window.scrollY;
    const right = document.body.offsetWidth - boundingTarget.right;
    setTransform({ top, right });
    setShow((prevShow) => !prevShow);
  };

  const isMobile = detectingDevice.isMobile();

  return (
    <Fragment>
      <div ref={(r) => (targetRef = r)} onClick={onToggleTarget}>
        {target}
      </div>
      {show &&
        ReactDOM.createPortal(
          <div
            ref={(r) => (modalBodyRef = r)}
            className='z-index-1024'
            style={
              isMobile && mobileCenter
                ? { top: transform.top, ...styleCenter }
                : {
                    position: 'absolute',
                    top: transform.top,
                    right: transform.right,
                  }
            }>
            {modalBody || children}
          </div>,
          document.body
        )}
    </Fragment>
  );
}

export default ModalDropdown;
