import React, { useEffect, useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import { elementUtils } from '../../utils/elementUtils';

function Select({ children, className, onChange, value = null }) {
  const [transform, setTransform] = useState({ top: 0, left: 0 });
  const [width, setWidth] = useState(0);
  const [show, setShow] = useState(false);

  const [state, setState] = useState({ value: value, label: '' });

  let targetRef = useRef();
  let optionBodyRef = useRef();

  useEffect(() => {
    React.Children.map(children, (child) => {
      if (React.isValidElement(child)) {
        if (value === child.props.value) {
          setState({ value: value, label: child.props.children });
        }
      }
    });
  }, [value]);

  useEffect(() => {
    document.addEventListener('click', onClickOutSideOptionBody);
    return () => {
      document.removeEventListener('click', onClickOutSideOptionBody);
    };
  });

  const onClickOutSideOptionBody = (evt) => {
    if (optionBodyRef instanceof HTMLElement) {
      const isClickInsideModal = elementUtils.isDescendantElement(
        optionBodyRef,
        evt.target
      );
      const isClickTarget = elementUtils.isDescendantElement(
        targetRef,
        evt.target
      );
      if (!isClickInsideModal && !isClickTarget) {
        setShow((prevShow) => (prevShow = false));
      }
    }
  };

  const onToggleTarget = () => {
    const boundingTarget = targetRef.getBoundingClientRect();

    const top = boundingTarget.bottom + window.scrollY;
    const left = boundingTarget.left;
    setTransform({ top, left });
    setShow((prevShow) => !prevShow);
    setWidth((prevWidth) => (prevWidth = targetRef.offsetWidth));
  };

  const onClickOption = (value, label) => {
    setState((prevState) => (prevState = { ...prevState, value, label }));
    setShow((prevShow) => (prevShow = false));
    if (typeof onChange === 'function') {
      onChange(value);
    }
  };

  let classes = ['select2-selection select2-selection--single'];
  if (className) {
    classes.push(className);
  }
  if (show) {
    classes.push('select-active');
  }
  return (
    <React.Fragment>
      <span
        ref={(r) => (targetRef = r)}
        onClick={onToggleTarget}
        className='select2 select2-container select2-container--default select2-container--below'>
        <span className='selection'>
          <span className={classes.join(' ')} tabIndex='0'>
            <span className='select2-selection__rendered'>{state.label}</span>
            <span className='select2-selection__arrow' role='presentation'>
              <b role='presentation'></b>
            </span>
          </span>
        </span>
        <span className='dropdown-wrapper' aria-hidden='true'></span>
      </span>

      {show &&
        ReactDOM.createPortal(
          <span
            ref={(r) => (optionBodyRef = r)}
            style={{
              position: 'absolute',
              top: transform.top,
              left: transform.left,
              width,
            }}
            className='select2-container select2-container--default select2-container--open'>
            <span
              className='select2-dropdown select2-dropdown--below position-relative'
              dir='ltr'>
              <span className='select2-search select2-search--dropdown select2-search--hide'>
                <input
                  className='select2-search__field'
                  type='search'
                  tabIndex='0'
                  role='searchbox'
                />
              </span>
              <span className='select2-results'>
                <ul
                  className='select2-results__options'
                  role='listbox'
                  aria-expanded='true'
                  aria-hidden='false'>
                  {React.Children.map(children, (child) => {
                    if (React.isValidElement(child)) {
                      return React.cloneElement(child, {
                        onClick: onClickOption,
                        active:
                          child.props.value &&
                          child.props.value === state.value,
                      });
                    }
                  })}
                </ul>
              </span>
            </span>
          </span>,
          document.body
        )}
    </React.Fragment>
  );
}

export default Select;
