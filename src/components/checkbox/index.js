import React from 'react';
import { v4 as uuid } from 'uuid';

function CheckBox({ className, label, error, checked, onChange }) {
  let classes = ['custom-control custom-control-sm custom-checkbox notext'];
  if (className) {
    classes.push(className);
  }

  const onChangeCheckbox = () => {
    if (typeof onChange === 'function') {
      onChange();
    }
  };

  const id = uuid();
  return (
    <div className={classes.join(' ')}>
      <input
        type='checkbox'
        className='custom-control-input'
        id={id}
        checked={checked ? true : false}
        readOnly
      />
      <label
        className='custom-control-label checked'
        htmlFor={id}
        onClick={onChangeCheckbox}>
        {label}
      </label>
      {error && (
        <span id='fv-com-error' className='invalid'>
          This field is required.
        </span>
      )}
    </div>
  );
}

export default CheckBox;
