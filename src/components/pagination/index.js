import React, { useState } from 'react';
import PaginationItem from './pagination_item';

const MAX_ITEM = 5;

function Pagination({ max, count, page, onChangePage }) {
  const [active, setActive] = useState(page ? page : 1);

  const maxItem = max ? max : MAX_ITEM;
  const itemsLength = count < maxItem ? count : maxItem;

  const onChangeItem = (value) => {
    setActive((prevActive) => (prevActive = value));
    if (typeof onChangePage === 'function') {
      onChangePage(value);
    }
  };

  const onClickNextPage = () => {
    if (active + 1 <= count) {
      setActive((prevActive) => {
        const next = prevActive + 1;
        if (typeof onChangePage === 'function') {
          onChangePage(next);
        }
        return next;
      });
    }
  };

  const onClickPrevPage = () => {
    if (active - 1 > 0) {
      setActive((prevActive) => {
        const prev = prevActive - 1;
        if (typeof onChangePage === 'function') {
          onChangePage(prev);
        }
        return prev;
      });
    }
  };

  const renderItem = () => {
    const spacing = Math.round(maxItem / 2);
    let jump = 1;
    if (count - active >= spacing && active - spacing > 0) {
      jump = active - spacing + 1;
    }

    if (count - active <= spacing && count - active >= 0) {
      jump = active - (maxItem - (count - active)) + 1;
    }

    jump = jump > 0 ? jump : 1;

    const items = [];
    for (let i = 0; i < itemsLength; i++) {
      items.push(
        <PaginationItem
          key={i}
          label={i + jump}
          active={active === i + jump}
          onClick={onChangeItem}
        />
      );
    }

    return items;
  };

  return (
    <div className='g'>
      <ul className='pagination justify-content-center justify-content-md-start'>
        <PaginationItem label='Trước' onClick={onClickPrevPage} />
        {renderItem()}
        <PaginationItem label='Tiếp' onClick={onClickNextPage} />
      </ul>
    </div>
  );
}

export default Pagination;
