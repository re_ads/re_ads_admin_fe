import React from 'react';
import { Link } from 'react-router-dom';

function MenuItem({ title, icon, link }) {
  return (
    <li className='nk-menu-item'>
      <Link to={link ? link : '#'} className='nk-menu-link'>
        <span className='nk-menu-icon'>{icon && icon}</span>
        <span className='nk-menu-text'>{title}</span>
      </Link>
    </li>
  );
}

export default MenuItem;
