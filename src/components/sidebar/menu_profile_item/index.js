import React from 'react';
import { Link } from 'react-router-dom';

function MenuProfileItem({ title, icon }) {
  return (
    <li>
      <Link to='#'>
        {icon && icon}
        <span>{title}</span>
      </Link>
    </li>
  );
}

export default MenuProfileItem;
