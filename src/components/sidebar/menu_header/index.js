import React from 'react';

function MenuHeader({ title }) {
  return (
    <li className='nk-menu-heading'>
      <h6 className='overline-title'>{title}</h6>
    </li>
  );
}

export default MenuHeader;
