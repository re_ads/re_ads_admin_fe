import React from 'react';
import { Link } from 'react-router-dom';
import { link } from '../../../consts/linkPage';
import MenuHeader from '../menu_header';
import MenuItem from '../menu_item';

function SidebarListMenu(props) {
  return (
    <div className='nk-sidebar-menu'>
      <ul className='nk-menu'>
        <MenuHeader title='Menu' />
        <MenuItem
          icon={<em className='icon ni ni-dashboard' />}
          title='Dashboard'
        />
        <MenuHeader title='Quản lý người dùng' />
        <MenuItem
          icon={<em className='icon ni ni-users' />}
          title='Danh sách người dùng'
          link={link.USER_MANAGE}
        />
        <MenuHeader title='Quản lý bài viết' />
        <MenuItem
          icon={<em className='icon ni ni-file-docs' />}
          title='Danh sách bài viết'
        />
        <MenuItem
          icon={<em className='icon ni ni-list-check'></em>}
          title='Bài viết cần phê duyệt'
        />
        <MenuItem
          icon={<em className='icon ni ni-edit-alt' />}
          title='Viết bài mới'
          link={link.CREATE_POST}
        />

        <MenuHeader title='Quản lý danh mục' />
        <MenuItem
          icon={<em className='icon ni ni-file-docs' />}
          title='Danh sách danh mục'
        />
      </ul>
    </div>
  );
}

export default SidebarListMenu;
