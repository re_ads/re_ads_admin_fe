import { configureStore } from '@reduxjs/toolkit';
import userManageReducer from '../pages/user_manage/userManageSlice';
import loginReducer from '../pages/login/loginSlice';
import userManageListReducer from '../pages/user_manage/user_manage_list/userManageListSlice';
import userManageDetailReducer from '../pages/user_manage/user_manage_detail/userManageDetailSlice';
import userManageEditReducer from '../pages/user_manage/user_manage_edit/userManageEditSlice';
// page create post
import postCreateReducer from '../pages/post_create/postCreateSlice';

export default configureStore({
  reducer: {
    userManage: userManageReducer,
    userManageList: userManageListReducer,
    userManageDetail: userManageDetailReducer,
    userManageEdit: userManageEditReducer,
    postCreate: postCreateReducer,
    login: loginReducer,
  },
});
