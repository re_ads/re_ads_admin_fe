import { lazy } from 'react';

const route = {
  path: '/create-post',
  exact: true,
  public: false,
  component: lazy(() => import('.')),
};

export default route;
