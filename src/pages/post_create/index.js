import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import ButtonIcon from '../../components/button_icon';
import Input from '../../components/input';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { useDispatch, useSelector } from 'react-redux';
import {
  createPost,
  getCategoryList,
  resetPostData,
  selectPostCreateState,
  updatePostData,
} from './postCreateSlice';
import Select from '../../components/select';
import Option from '../../components/select/option';
import { useError } from '../../custom_hook/useError';
import { v4 as uuid } from 'uuid';
import ModalError from '../../components/modal_error';
import { UPLOAD_URL } from '../../configs';

function PostCreate(props) {
  const [tag, setTag] = useState('');
  const [thumbnail, setThumbnail] = useState({ preview: '', raw: '' });
  const [errorCommon, setErrorCommon] = useError();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { post, categoryList } = useSelector(selectPostCreateState);
  // validate state
  const [errorCategory, setErrorCategory] = useError();
  const [errorTitle, setErrorTitle] = useError();
  const [errorMetaTitle, setErrorMetaTitle] = useError();
  const [errorSummary, setErrorSummary] = useError();
  const [errorContent, setErrorContent] = useError();

  let uploadThumbnailRef = useRef();

  useEffect(() => {
    dispatch(getCategoryList());
  }, []);

  const onChangeTitle = (evt) => {
    // TODO: validate
    const title = evt.target.value;
    dispatch(updatePostData({ title }));
  };

  const onChangeMetaTitle = (evt) => {
    // TODO: validate
    const metaTitle = evt.target.value;
    dispatch(updatePostData({ metaTitle }));
  };

  const onChangeSummary = (evt) => {
    // TODO: validate
    const summary = evt.target.value;
    dispatch(updatePostData({ summary }));
  };

  const onClickUploadThumbnail = (evt) => {
    if (uploadThumbnailRef instanceof HTMLElement) {
      uploadThumbnailRef.click();
    }
  };

  const onChangeImageThumbnail = (evt) => {
    if (evt.target.files.length) {
      const file = evt.target.files[0];
      if (file.type.indexOf('image/') !== 0) {
        setErrorCommon({
          error: true,
          message: t('post_create_thumbnail_error_type'),
        });
        return;
      }
      setThumbnail({
        preview: URL.createObjectURL(file),
        raw: file,
      });
    }
  };

  const onChangeContent = (evt, editor) => {
    const content = editor.getData();
    dispatch(updatePostData({ content }));
  };

  const onChangeCategory = (categoryId) => {
    dispatch(updatePostData({ categoryId }));
  };

  const onResetPost = () => {
    dispatch(resetPostData());
    setThumbnail('');
  };

  const onChangeTag = (evt) => {
    const tag = evt.target.value;
    setTag(tag);
  };

  const addTagsToPostData = (evt) => {
    evt.preventDefault();
    const tags = [...post.tags];
    if (tag.trim() === '') {
      return;
    }
    tags.push(tag);
    dispatch(updatePostData({ tags }));
    setTag('');
  };

  const removeTagFromTags = (index) => {
    const tags = [...post.tags];
    tags.splice(index, 1);
    dispatch(updatePostData({ tags }));
  };

  const validateCategory = () => {
    if (!post.categoryId) {
      setErrorCategory({
        error: true,
        message: t('post_create_category_error_empty'),
      });
      return false;
    }

    setErrorCategory({
      error: false,
      message: '',
    });
    return true;
  };

  const validateTitle = () => {
    if (!post.title.trim()) {
      setErrorTitle({
        error: true,
        message: t('post_create_title_error_empty'),
      });
      return false;
    }

    setErrorTitle({
      error: false,
      message: '',
    });
    return true;
  };

  const validateMetaTitle = () => {
    if (!post.metaTitle) {
      setErrorMetaTitle({
        error: true,
        message: t('post_create_meta_title_empty'),
      });
      return false;
    }
    setErrorMetaTitle({
      error: false,
      message: '',
    });
    return true;
  };

  const validateSummary = () => {
    if (!post.summary) {
      setErrorSummary({
        error: true,
        message: t('post_create_summary_empty'),
      });
      return false;
    }

    setErrorSummary({
      error: false,
      message: '',
    });
    return true;
  };

  const validateContent = () => {
    if (!post.content.trim()) {
      setErrorContent({
        error: true,
        message: t('post_create_content_empty'),
      });
      return false;
    }

    setErrorContent({
      error: false,
      message: '',
    });
    return true;
  };

  const validateAll = () => {
    const isValidCategory = validateCategory();
    const isValidTitle = validateTitle();
    const isValidMetaTitle = validateMetaTitle();
    const isValidSummary = validateSummary();
    const isValidContent = validateContent();
    if (
      isValidCategory &&
      isValidTitle &&
      isValidMetaTitle &&
      isValidSummary &&
      isValidContent
    ) {
      return true;
    }

    return false;
  };

  const onSubmitFormAddPost = () => {
    const isValid = validateAll();
    if (isValid) {
      dispatch(createPost(thumbnail.raw));
    }
  };

  const onCloseCommonError = () => {
    setErrorCommon({
      error: false,
      message: '',
    });
  };

  let categorySelectClasses = [''];
  let titleClasses = [''];
  let metaTitleClasses = [''];
  let summaryClasses = ['form-control form-control-sm'];
  if (errorCategory.error) {
    categorySelectClasses.push('form-invalid');
  }
  if (errorTitle.error) {
    titleClasses.push('form-invalid');
  }
  if (errorMetaTitle.error) {
    metaTitleClasses.push('form-invalid');
  }
  if (errorSummary.error) {
    summaryClasses.push('form-invalid');
  }
  if (errorContent.error) {
    const summaryRef = document.getElementsByClassName(
      'ck-editor__editable'
    )[0];
    summaryRef.classList.add('form-invalid');
  }

  return (
    <div>
      <div className='nk-block-head nk-block-head-sm'>
        <div className='nk-block-between-md g-3'>
          <div className='nk-block-head-content'>
            <h3 className='nk-block-title page-title'>
              {t('post_create_header')}
            </h3>
          </div>
          <div className='nk-block-head-content'>
            <ul className='nk-block-tools gx-3'>
              <li>
                <ButtonIcon
                  className='btn btn-outline-light bg-white d-sm-inline-flex'
                  icon={<em className='icon ni ni-reload-alt'></em>}
                  label={t('global_reset')}
                  onClick={onResetPost}
                />
              </li>
              <li>
                <ButtonIcon
                  className='btn btn-outline-light bg-white d-sm-inline-flex'
                  icon={<em className='icon ni ni-save'></em>}
                  label={t('global_add')}
                  onClick={onSubmitFormAddPost}
                />
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className='nk-block'>
        <div className='card card-bordered'>
          <div className='card-aside-wrap'>
            <div className='card-content width-100'>
              <div className='card-inner'>
                <div className='nk-block'>
                  <div className='nk-block-head'>
                    <h5 className='title'>{t('post_create_info_basic')}</h5>
                  </div>
                  <div className='form-validate'>
                    <div className='row g-gs'>
                      <div className='col-md-6'>
                        <div className='form-group'>
                          <label className='form-label'>
                            {t('post_create_category')}
                          </label>
                          <Select
                            className={categorySelectClasses.join(' ')}
                            onChange={onChangeCategory}
                            value={post.categoryId}>
                            <Option value={''}>
                              {t('post_create_category_placeholder')}
                            </Option>
                            {categoryList.map((category) => (
                              <Option key={category.id} value={category.id}>
                                {category.title}
                              </Option>
                            ))}
                          </Select>
                          <span className='invalid'>
                            {errorCategory.message}
                          </span>
                        </div>
                      </div>
                      <div className='col-md-6'>
                        <div className='form-group'>
                          <label className='form-label'>
                            {t('post_create_title')}
                          </label>
                          <Input
                            className={titleClasses.join(' ')}
                            error={errorTitle.message}
                            value={post.title}
                            onChange={onChangeTitle}
                          />
                        </div>
                      </div>
                      <div className='col-md-6'>
                        <div className='form-group'>
                          <label className='form-label'>
                            {t('post_create_meta_title')}
                          </label>
                          <Input
                            className={metaTitleClasses.join(' ')}
                            error={errorMetaTitle.message}
                            value={post.metaTitle}
                            onChange={onChangeMetaTitle}
                          />
                        </div>
                      </div>
                      <div className='col-md-12'>
                        <div className='form-group'>
                          <label className='form-label'>
                            {t('post_create_summary')}
                          </label>
                          <div className='form-control-wrap'>
                            <textarea
                              className={summaryClasses.join(' ')}
                              required=''
                              placeholder={t('post_create_summary_placeholder')}
                              value={post.summary ? post.summary : ''}
                              onChange={onChangeSummary}
                            />
                            <span className='invalid'>
                              {errorSummary.message}
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className='col-md-12'>
                        <div className='col-md-3 p-0'>
                          <label className='form-label'>
                            {t('post_create_thumbnail')}
                          </label>
                          <div className='card card-bordered card-preview'>
                            <div className='card-inner'>
                              <img
                                src={thumbnail.preview}
                                alt='post thumbnail'
                                loading='lazy'
                              />
                            </div>
                          </div>
                          <div className='mt-2'>
                            <input
                              ref={(r) => (uploadThumbnailRef = r)}
                              type='file'
                              className='d-none'
                              onChange={onChangeImageThumbnail}
                            />
                            <ButtonIcon
                              className='btn btn-outline-light bg-white d-sm-inline-flex width-100'
                              label={t('post_create_thumbnail_btn_upload')}
                              onClick={onClickUploadThumbnail}
                            />
                          </div>
                        </div>
                      </div>
                      <div className='col-md-12'>
                        <div className='form-group'>
                          <label className='form-label'>
                            {t('post_create_content')}
                          </label>
                          <div className='form-control-wrap'>
                            <CKEditor
                              editor={ClassicEditor}
                              config={{
                                ckfinder: {
                                  uploadUrl: UPLOAD_URL,
                                },
                              }}
                              data={post.content}
                              onChange={onChangeContent}
                            />
                            <span className='invalid'>
                              {errorContent.message}
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className='col-md-6'>
                        <form onSubmit={addTagsToPostData}>
                          <div className='form-group'>
                            <label className='form-label'>
                              {t('post_create_tag')}
                            </label>
                            <Input
                              type='text'
                              value={tag}
                              onChange={onChangeTag}
                              placeholder={t('post_create_tags_placeholder')}
                            />
                            <button
                              className='d-none'
                              type='submit'
                              onClick={addTagsToPostData}></button>
                          </div>
                        </form>
                      </div>
                      <div className='col-md-12'>
                        <ul className='preview-list'>
                          {post.tags.map((tag, index) => (
                            <li key={uuid()} className='preview-item'>
                              <span className='badge badge-sm badge-pill badge-outline'>
                                {tag}
                                <span
                                  className='ml-1 remove-icon-hex'
                                  onClick={() => removeTagFromTags(index)}>
                                  &#10005;
                                </span>
                              </span>
                            </li>
                          ))}
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ModalError
        show={errorCommon.error}
        message={errorCommon.message}
        onClose={onCloseCommonError}
      />
    </div>
  );
}

export default PostCreate;
