import { createSlice } from '@reduxjs/toolkit';
import { services } from '../../services/api';

const defaultPost = {
  title: '',
  metaTitle: '',
  summary: '',
  content: '',
  mapLatitude: null,
  mapLongitude: null,
  tags: [],
  categoryId: '',
};

export const postCreateSlice = createSlice({
  name: 'postCreate',
  initialState: {
    post: defaultPost,
    categoryList: [],
  },
  reducers: {
    updatePostData(state, action) {
      state.post = { ...state.post, ...action.payload };
    },
    resetPostData(state, action) {
      state.post = { ...state.post, ...defaultPost };
    },
    getCategoryListSuccess(state, action) {
      state.categoryList = action.payload;
    },
  },
});

export const {
  updatePostData,
  resetPostData,
  getCategoryListSuccess,
} = postCreateSlice.actions;

export const selectPostCreateState = (state) => state.postCreate;

export const getCategoryList = () => {
  return (dispatch) => {
    services.categoryService.getCategoryList().then((res) => {
      dispatch(getCategoryListSuccess(res.data));
    });
  };
};

export const createPost = (thumbnail) => {
  return (dispatch, getState) => {
    const state = getState().postCreate;
    const { post } = state;
    services.postService.createPost({ ...post, thumbnail }).then((res) => {});
  };
};
export default postCreateSlice.reducer;
