import { createSlice } from '@reduxjs/toolkit';
import { services } from '../../services/api';

export const userManageSlice = createSlice({
  name: 'userManage',
  initialState: {
    currentPage: '',
    currentUserId: '',
  },
  reducers: {
    setCurrentPageForUserManage(state, action) {
      state.currentPage = action.payload;
    },
    setCurrentUserId(state, action) {
      state.currentUserId = action.payload;
    },
  },
});

export const {
  setCurrentPageForUserManage,
  setCurrentUserId,
} = userManageSlice.actions;

export const selectUserManageState = (state) => state.userManage;

export default userManageSlice.reducer;
