import { lazy } from 'react';

const route = {
  path: '/user-manage',
  exact: true,
  public: false,
  component: lazy(() => import('.')),
};

export default route;
