import { createSlice } from '@reduxjs/toolkit';
import { services } from '../../../services/api';

export const userManageEditSlice = createSlice({
  name: 'userManageEdit',
  initialState: {
    roles: [],
    userEditData: {},
  },
  reducers: {
    getRolesSuccess(state, action) {
      state.roles = action.payload;
    },
    updateUserEditData(state, action) {
      const user = action.payload;
      state.userEditData = { ...state.userEditData, ...user };
    },
  },
});

export const {
  getRolesSuccess,
  updateUserEditData,
} = userManageEditSlice.actions;

export const selectUserManageEditState = (state) => state.userManageEdit;

export const getRoles = () => {
  return (dispatch) => {
    services.userManageService.getRoles().then((res) => {
      dispatch(getRolesSuccess(res.data));
    });
  };
};

export default userManageEditSlice.reducer;
