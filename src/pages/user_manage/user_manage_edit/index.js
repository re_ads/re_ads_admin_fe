import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import ButtonIcon from '../../../components/button_icon';
import CheckBox from '../../../components/checkbox';
import Input from '../../../components/input';
import { PAGE_USER_MANAGE_DETAIL } from '../pages.consts';
import { setCurrentPageForUserManage } from '../userManageSlice';
import {
  selectUserManageEditState,
  getRoles,
  updateUserEditData,
} from './userManageEditSlice';
import { selectUserManageDetailState } from '../user_manage_detail/userManageDetailSlice';

function UserManageEdit(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { roles, userEditData } = useSelector(selectUserManageEditState);
  const { user } = useSelector(selectUserManageDetailState);

  const onBackToUserManageDetail = () => {
    dispatch(setCurrentPageForUserManage(PAGE_USER_MANAGE_DETAIL));
  };

  useEffect(() => {
    dispatch(getRoles());
    dispatch(updateUserEditData(user));
  }, []);

  const onChangeFirstName = (evt) => {
    // TODO: validate
    const firstName = evt.target.value;
    dispatch(updateUserEditData({ firstName }));
  };

  const onChangeLastName = (evt) => {
    // TODO: validate
    const lastName = evt.target.value;
    dispatch(updateUserEditData({ lastName }));
  };

  const onChangeMobile = (evt) => {
    // TODO: validate
    const mobile = evt.target.value;
    dispatch(updateUserEditData({ mobile }));
  };

  const onChangeProfile = (evt) => {
    // TODO: validate
    const profile = evt.target.value;
    dispatch(updateUserEditData({ profile }));
  };

  const onChangeRoles = () => {
    // TODO: add onChange roles
  };

  const renderRoles = () => {
    const userRoleList = [...user.roles];
    let userRoleListString = [];
    for (const userRole of userRoleList) {
      userRoleListString.push(userRole.name);
    }

    return roles.map((role) => (
      <li key={role.id}>
        <CheckBox
          className='custom-control custom-checkbox'
          label={role.name}
          checked={userRoleListString.includes(role.name)}
          onChange={onChangeRoles}
        />
      </li>
    ));
  };

  return (
    <React.Fragment>
      <div className='nk-block-head nk-block-head-sm'>
        <div className='nk-block-between g-3'>
          <div className='nk-block-head-content'>
            <h3 className='nk-block-title page-title'>
              {t('user_manage_edit_header')}
            </h3>
          </div>
          <div className='nk-block-head-content'>
            <ButtonIcon
              className='btn btn-outline-light bg-white d-none d-sm-inline-flex'
              icon={<em className='icon ni ni-arrow-left' />}
              label={t('global_back')}
              onClick={onBackToUserManageDetail}
            />
            <ButtonIcon
              className='btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none'
              icon={<em className='icon ni ni-arrow-left'></em>}
              onClick={onBackToUserManageDetail}
            />
          </div>
        </div>
      </div>
      <div className='card-inner card-bordered'>
        <form className='form-validate'>
          <div className='row g-gs'>
            <div className='col-md-6'>
              <div className='form-group'>
                <label className='form-label'>
                  {t('user_manage_edit_firstname')}
                </label>
                <Input
                  value={userEditData.firstName}
                  onChange={onChangeFirstName}
                />
              </div>
            </div>
            <div className='col-md-6'>
              <div className='form-group'>
                <label className='form-label'>
                  {t('user_manage_edit_lastname')}
                </label>
                <Input
                  value={userEditData.lastName}
                  onChange={onChangeLastName}
                />
              </div>
            </div>
            <div className='col-md-6'>
              <div className='form-group'>
                <label className='form-label'>{t('global_phone')}</label>
                <Input value={userEditData.mobile} onChange={onChangeMobile} />
              </div>
            </div>
            <div className='col-md-12'>
              <div className='form-group'>
                <label className='form-label'>
                  {t('user_manage_edit_profile')}
                </label>
                <div className='form-control-wrap'>
                  <textarea
                    className='form-control form-control-sm invalid'
                    placeholder='Write your message'
                    required=''
                    value={userEditData.profile ? userEditData.profile : ''}
                    onChange={onChangeProfile}
                  />
                  <span className='invalid'>This field is required.</span>
                </div>
              </div>
            </div>
            <div className='col-md-12'>
              <div className='form-group'>
                <label className='form-label'>
                  {t('user_manage_edit_role')}
                </label>
                <ul className='custom-control-group g-3 align-center'>
                  {renderRoles()}
                </ul>
              </div>
            </div>
            <div className='col-md-12'>
              <div className='form-group'>
                <button type='submit' className='btn btn-lg btn-primary'>
                  {t('global_save')}
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </React.Fragment>
  );
}

export default UserManageEdit;
