import { createSlice } from '@reduxjs/toolkit';
import { services } from '../../../services/api';

const defaultUser = {
  roles: [],
};
export const userManageDetailSlice = createSlice({
  name: 'userManageDetail',
  initialState: {
    user: defaultUser,
  },
  reducers: {
    getUserDetailSuccess(state, action) {
      state.user = action.payload;
    },
  },
});

export const { getUserDetailSuccess } = userManageDetailSlice.actions;

export const selectUserManageDetailState = (state) => state.userManageDetail;

export const getUserDetail = (userId) => {
  return (dispatch) => {
    services.userManageService.getUserDetail(userId).then((res) => {
      dispatch(getUserDetailSuccess(res.attributes));
    });
  };
};

export default userManageDetailSlice.reducer;
