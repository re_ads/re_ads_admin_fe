import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import ButtonIcon from '../../../components/button_icon';
import { PAGE_USER_MANAGE_EDIT, PAGE_USER_MANAGE_LIST } from '../pages.consts';
import {
  selectUserManageState,
  setCurrentPageForUserManage,
} from '../userManageSlice';
import { selectUserManageDetailState } from './userManageDetailSlice';
import InfomationItem from './components/infomation_item';
import { getUserDetail } from './userManageDetailSlice';
import { dateUtils } from '../../../utils/dateUtils';

function UserManageDetail(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const onBackToUserManageList = () => {
    dispatch(setCurrentPageForUserManage(PAGE_USER_MANAGE_LIST));
  };

  const { currentUserId } = useSelector(selectUserManageState);
  const { user } = useSelector(selectUserManageDetailState);

  useEffect(() => {
    dispatch(getUserDetail(currentUserId));
  }, []);

  const onClickButtonEdit = () => {
    dispatch(setCurrentPageForUserManage(PAGE_USER_MANAGE_EDIT));
  };

  return (
    <React.Fragment>
      <div className='nk-block-head nk-block-head-sm'>
        <div className='nk-block-between-md g-4'>
          <div className='nk-block-head-content'>
            <h3 className='nk-block-title page-title'>
              {t('global_user')} /{' '}
              <strong className='text-primary small'>
                {user.firstName + ' ' + user.lastName}
              </strong>
            </h3>
            <div className='nk-block-des text-soft'>
              <ul className='list-inline'>
                <li>
                  {t('user_manage_detail_user_id')}:{' '}
                  <span className='text-base'>{user.id}</span>
                </li>
                <li>
                  {t('user_manage_detail_last_login')}:{' '}
                  <span className='text-base'>
                    {dateUtils.formatddMMyyyyHHmmss(user.lastLogin)}
                  </span>
                </li>
              </ul>
            </div>
          </div>
          <div className='nk-block-head-content'>
            <ul className='nk-block-tools gx-3'>
              <li>
                <ButtonIcon
                  className='btn btn-outline-light bg-white d-sm-inline-flex'
                  icon={<em className='icon ni ni-arrow-left' />}
                  label={t('global_back')}
                  onClick={onBackToUserManageList}
                />
              </li>
              <li>
                <ButtonIcon
                  className='btn btn-outline-light bg-white d-sm-inline-flex'
                  icon={<em className='icon ni ni-edit'></em>}
                  label={t('user_manage_detail_btn_edit')}
                  onClick={onClickButtonEdit}
                />
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className='nk-block'>
        <div className='card card-bordered'>
          <div className='card-aside-wrap'>
            <div className='card-content'>
              <ul className='nav nav-tabs nav-tabs-mb-icon nav-tabs-card'>
                <li className='nav-item'>
                  <ButtonIcon
                    className='nav-link active'
                    icon={<em className='icon ni ni-user-circle' />}
                    label={t('user_manage_detail_tab_personal')}
                  />
                </li>
              </ul>
              <div className='card-inner'>
                <div className='nk-block'>
                  <div className='nk-block-head'>
                    <h5 className='title'>{t('global_personal_infomation')}</h5>
                  </div>

                  <div className='profile-ud-list'>
                    <InfomationItem
                      label={t('user_manage_detail_fullname')}
                      value={user.firstName + ' ' + user.lastName}
                    />
                    <InfomationItem
                      label={t('user_manage_detail_firstname')}
                      value={user.firstName}
                    />
                    <InfomationItem
                      label={t('global_phone')}
                      value={user.mobile}
                    />
                    <div className='profile-ud-item'>
                      <div className='profile-ud wider'>
                        <span className='profile-ud-label'>
                          {t('global_email')}
                        </span>
                        <span className='profile-ud-value'>{user.email}</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className='nk-block'>
                  <div className='nk-block-head nk-block-head-line'>
                    <h6 className='title overline-title text-base'>
                      {t('user_manage_detail_more_info')}
                    </h6>
                  </div>
                  <div className='profile-ud-list'>
                    <InfomationItem
                      label={t('global_registered_at')}
                      value={dateUtils.formatddMMyyyyHHmmss(user.registeredAt)}
                    />
                    <InfomationItem
                      label={t('user_manage_detail_last_login')}
                      value={dateUtils.formatddMMyyyyHHmmss(user.lastLogin)}
                    />
                    <InfomationItem
                      label={t('user_manage_detail_role')}
                      value={user.roles.map((role) => role.name).join(', ')}
                    />
                    <InfomationItem
                      label={t('global_status')}
                      value={user.statusName}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default UserManageDetail;
