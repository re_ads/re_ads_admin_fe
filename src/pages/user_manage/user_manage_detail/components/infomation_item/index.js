import React from 'react';

function InfomationItem({ label, value }) {
  return (
    <div className='profile-ud-item'>
      <div className='profile-ud wider'>
        <span className='profile-ud-label'>{label}</span>
        <span className='profile-ud-value'>{value}</span>
      </div>
    </div>
  );
}

export default InfomationItem;
