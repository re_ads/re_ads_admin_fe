import React, { useState } from 'react';
import UserManageDetail from './user_manage_detail';
import UsersManageList from './user_manage_list';

import UserManageAdd from './user_manage_add';
import UserManageEdit from './user_manage_edit';
import { useDispatch, useSelector } from 'react-redux';
import { selectUserManageState } from './userManageSlice';
import {
  PAGE_USER_MANAGE_ADD,
  PAGE_USER_MANAGE_DETAIL,
  PAGE_USER_MANAGE_EDIT,
  PAGE_USER_MANAGE_LIST,
} from './pages.consts';

function UserManage(props) {
  const defaultPage = UsersManageList;
  const { currentPage } = useSelector(selectUserManageState);

  const listPage = [
    {
      name: PAGE_USER_MANAGE_LIST,
      component: UsersManageList,
      props: {},
    },
    {
      name: PAGE_USER_MANAGE_DETAIL,
      component: UserManageDetail,
      props: {},
    },
    {
      name: PAGE_USER_MANAGE_ADD,
      component: UserManageAdd,
      props: {},
    },
    {
      name: PAGE_USER_MANAGE_EDIT,
      component: UserManageEdit,
      props: {},
    },
  ];

  const addPropsForPage = () => {
    if (currentPage === PAGE_USER_MANAGE_LIST) {
      return {};
    }
  };

  const renderPage = (props) => {
    let Page = defaultPage;
    let pageProps = addPropsForPage();
    for (const page of listPage) {
      if (page.name === currentPage) {
        Page = page.component;
        if (Object.prototype.hasOwnProperty.call(page, 'props')) {
          pageProps = { ...page.props, ...pageProps };
        }
      }
    }

    return <Page {...props} {...pageProps} />;
  };

  return <div>{renderPage()}</div>;
}

export default UserManage;
