import React from 'react';
import ButtonIcon from '../../../components/button_icon';
import CheckBox from '../../../components/checkbox';
import Input from '../../../components/input';
import Select from '../../../components/select';
import Option from '../../../components/select/option';

function UserManageAdd(props) {
  return (
    <React.Fragment>
      <div className='nk-block-head nk-block-head-sm'>
        <div className='nk-block-between g-3'>
          <div className='nk-block-head-content'>
            <h3 className='nk-block-title page-title'>Thêm người dùng</h3>
          </div>
          <div className='nk-block-head-content'>
            <ButtonIcon
              className='btn btn-outline-light bg-white d-none d-sm-inline-flex'
              icon={<em className='icon ni ni-arrow-left' />}
              label='Quay lại'
            />
            <ButtonIcon
              className='btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none'
              icon={<em className='icon ni ni-arrow-left'></em>}
            />
          </div>
        </div>
      </div>
      <div class='card-inner card-bordered'>
        <form
          action='#'
          class='form-validate'
          novalidate='novalidate'
          data-select2-id='6'>
          <div class='row g-gs'>
            <div class='col-md-6'>
              <div class='form-group'>
                <label class='form-label' for='fv-full-name'>
                  Full Name
                </label>
                <Input />
              </div>
            </div>
            <div class='col-md-6'>
              <div class='form-group'>
                <label class='form-label' for='fv-email'>
                  Email address
                </label>
                <Input />
              </div>
            </div>
            <div class='col-md-6'>
              <div class='form-group'>
                <label class='form-label' for='fv-subject'>
                  Subject
                </label>
                <Input />
              </div>
            </div>
            <div class='col-md-6'>
              <div class='form-group'>
                <label class='form-label' for='fv-topics'>
                  Topics
                </label>
                <div class='form-control-wrap'>
                  <Select
                    className='form-control form-select invalid'
                    value={1}>
                    <Option value={1}>dsfsd</Option>
                  </Select>
                  <span class='invalid'>This field is required.</span>
                </div>
              </div>
            </div>
            <div class='col-md-12'>
              <div class='form-group'>
                <label class='form-label' for='fv-message'>
                  Message
                </label>
                <div class='form-control-wrap'>
                  <textarea
                    class='form-control form-control-sm invalid'
                    placeholder='Write your message'
                    required=''
                    aria-describedby='fv-message-error'></textarea>
                  <span class='invalid'>This field is required.</span>
                </div>
              </div>
            </div>
            <div class='col-md-12'>
              <div class='form-group'>
                <label class='form-label'>Communication</label>
                <ul class='custom-control-group g-3 align-center'>
                  <li>
                    <CheckBox
                      className='custom-control custom-checkbox'
                      label='Email'
                      error={true}
                    />
                  </li>
                  <li>
                    <CheckBox
                      className='custom-control custom-checkbox'
                      label='Email'
                      error={false}
                    />
                  </li>
                </ul>
              </div>
            </div>
            <div class='col-md-12'>
              <div class='form-group'>
                <button type='submit' class='btn btn-lg btn-primary'>
                  Lưu
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </React.Fragment>
  );
}

export default UserManageAdd;
