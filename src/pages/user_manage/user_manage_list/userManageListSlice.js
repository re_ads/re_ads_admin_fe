import { createSlice } from '@reduxjs/toolkit';
import { services } from '../../../services/api';
import { userStatus } from '../../../consts/userStatus';

export const userManageListSlice = createSlice({
  name: 'userManageList',
  initialState: {
    listUsers: [],
    currentPage: 1,
    totalPages: 0,
    page: {
      number: 1,
      size: 10,
    },
    filters: {},
    sort: {},
  },
  reducers: {
    getListUserSuccess(state, action) {
      state.listUsers = action.payload.data;
      state.totalPages = action.payload.meta.totalPages;
    },
    setPageNumber(state, action) {
      state.page.number = action.payload;
    },
  },
});

export const {
  getListUserSuccess,
  setPageNumber,
} = userManageListSlice.actions;

export const selectUserManageListState = (state) => state.userManageList;

export const getListUsers = () => {
  return (dispatch, getState) => {
    const state = getState().userManageList;
    const { page, filters, sort } = state;
    services.userManageService.getListUsers(page, filters, sort).then((res) => {
      dispatch(getListUserSuccess(res));
    });
  };
};

export const lockUser = (userId) => {
  return (dispatch) => {
    services.userManageService
      .updateUserStatus(userId, userStatus.UNACTIVE_NAME)
      .then((res) => {
        dispatch(getListUsers());
      });
  };
};

export const deleteUser = (userId) => {
  return (dispatch) => {
    services.userManageService
      .updateUserStatus(userId, userStatus.DELETED_NAME)
      .then((res) => {
        dispatch(getListUsers());
      });
  };
};

export default userManageListSlice.reducer;
