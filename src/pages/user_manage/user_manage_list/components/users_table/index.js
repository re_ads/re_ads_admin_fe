import React from 'react';
import { useSelector } from 'react-redux';
import ButtonIcon from '../../../../../components/button_icon';
import CheckBox from '../../../../../components/checkbox';
import ModalDropdown from '../../../../../components/modal_dropdown';
import { selectUserManageListState } from '../../userManageListSlice';
import UsersModalAction from '../users_modal_action';
import { useTranslation } from 'react-i18next';
import { userStatus } from '../../../../../consts/userStatus';
import { dateUtils } from '../../../../../utils/dateUtils';

function UsersTable(props) {
  const { t } = useTranslation();
  const { listUsers } = useSelector(selectUserManageListState);

  return (
    <div className='nk-tb-list nk-tb-ulist'>
      <div className='nk-tb-item nk-tb-head'>
        <div className='nk-tb-col nk-tb-col-check'>
          <CheckBox />
        </div>
        <div className='nk-tb-col'>
          <span className='sub-text'>{t('global_user')}</span>
        </div>
        <div className='nk-tb-col tb-col-md'>
          <span className='sub-text'>{t('global_phone')}</span>
        </div>
        <div className='nk-tb-col tb-col-lg'>
          <span className='sub-text'>{t('global_registered_at')}</span>
        </div>
        <div className='nk-tb-col tb-col-lg'>
          <span className='sub-text'>{t('global_last_login')}</span>
        </div>
        <div className='nk-tb-col tb-col-md'>
          <span className='sub-text'>{t('global_status')}</span>
        </div>
        <div className='nk-tb-col nk-tb-col-tools text-right'></div>
      </div>
      {listUsers.map((user) => (
        <div key={user.id} className='nk-tb-item'>
          <div className='nk-tb-col nk-tb-col-check'>
            <CheckBox />
          </div>
          <div className='nk-tb-col'>
            <div>
              <div className='user-card'>
                <div className='user-avatar bg-primary'>
                  <span>AB</span>
                </div>
                <div className='user-info'>
                  <span className='tb-lead'>
                    {user.firstName} {user.lastName}
                    <span className='dot dot-success d-md-none ml-1'></span>
                  </span>
                  <span>{user.email}</span>
                </div>
              </div>
            </div>
          </div>
          <div className='nk-tb-col tb-col-mb'>
            <span>{user.mobile}</span>
          </div>
          <div className='nk-tb-col tb-col-md'>
            <span>{dateUtils.fomatddMMyyyy(user.registeredAt)}</span>
          </div>
          <div className='nk-tb-col tb-col-lg'>
            <span>{dateUtils.fomatddMMyyyy(user.lastLogin)}</span>
          </div>
          <div className='nk-tb-col tb-col-md'>
            {user.status === userStatus.ACTIVE && (
              <span className='tb-status text-success'>{user.statusName}</span>
            )}
            {user.status === userStatus.UNACTIVE && (
              <span className='tb-status text-warning'>{user.statusName}</span>
            )}
            {user.status === userStatus.DELETED && (
              <span className='tb-status text-danger'>{user.statusName}</span>
            )}
          </div>
          <div className='nk-tb-col nk-tb-col-tools'>
            <ul className='nk-tb-actions gx-1'>
              <li>
                <div className='drodown'>
                  <ModalDropdown
                    target={
                      <ButtonIcon
                        className='dropdown-toggle btn btn-icon btn-trigger'
                        icon={<em className='icon ni ni-more-h'></em>}
                      />
                    }>
                    <UsersModalAction user={user} />
                  </ModalDropdown>
                </div>
              </li>
            </ul>
          </div>
        </div>
      ))}
    </div>
  );
}

export default UsersTable;
