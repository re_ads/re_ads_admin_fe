import React from 'react';
import { Link } from 'react-router-dom';

function UsersDropdownSetting(props) {
  return (
    <div className='dropdown-menu dropdown-menu-xs dropdown-menu-right show position-relative'>
      <ul className='link-check'>
        <li>
          <span>Hiển thị</span>
        </li>
        <li className='active'>
          <Link to='#'>10</Link>
        </li>
        <li>
          <Link to='#'>10</Link>
        </li>
        <li>
          <Link to='#'>10</Link>
        </li>
      </ul>
      <ul className='link-check'>
        <li>
          <span>Sắp xếp</span>
        </li>
        <li className='active'>
          <Link to='#'>Tăng dần</Link>
        </li>
        <li>
          <Link to='#'>Giảm dần</Link>
        </li>
      </ul>
    </div>
  );
}

export default UsersDropdownSetting;
