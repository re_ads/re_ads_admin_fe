import React from 'react';
import ButtonIcon from '../../../../../components/button_icon';

function UsersSearch({ active, onClose }) {
  const onCloseSearch = () => {
    if (typeof onClose === 'function') {
      onClose();
    }
  };

  let classes = ['card-search search-wrap'];
  if (active) {
    classes.push('active');
  }
  return (
    <div className={classes.join(' ')} data-search='search'>
      <div className='card-body'>
        <div className='search-content'>
          <ButtonIcon
            className='search-back btn btn-icon toggle-search'
            icon={<em className='icon ni ni-arrow-left'></em>}
            onClick={onCloseSearch}
          />
          <input
            type='text'
            className='form-control border-transparent form-focus-none'
            placeholder='Tìm kiếm theo tên hoặc email'
          />
          <button className='search-submit btn btn-icon'>
            <em className='icon ni ni-search'></em>
          </button>
        </div>
      </div>
    </div>
  );
}

export default UsersSearch;
