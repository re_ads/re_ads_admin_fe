import React from 'react';
import ButtonIcon from '../../../../../components/button_icon';
import Select from '../../../../../components/select';
import Option from '../../../../../components/select/option';

function UsersDropdownFilter(props) {
  return (
    <div className='filter-wg dropdown-menu dropdown-menu-xl dropdown-menu-right show position-relative'>
      <div className='dropdown-head'>
        <span className='sub-title dropdown-title'>Lọc người dùng</span>
        <div className='dropdown'>
          <ButtonIcon
            className='btn btn-sm btn-icon'
            icon={<em className='icon ni ni-more-h'></em>}
          />
        </div>
      </div>
      <div className='dropdown-body dropdown-body-rg'>
        <div className='row gx-6 gy-3'>
          <div className='col-6'>
            <div className='form-group'>
              <label className='overline-title overline-title-alt'>Role</label>
              <Select>
                <Option>Any Status</Option>
              </Select>
            </div>
          </div>
          <div className='col-6'>
            <div className='form-group'>
              <label className='overline-title overline-title-alt'>
                Status
              </label>
              <Select>
                <Option>Any Status</Option>
              </Select>
            </div>
          </div>
          <div className='col-12'>
            <div className='form-group'>
              <button type='button' className='btn btn-secondary'>
                Lọc
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className='dropdown-foot between'>
        <div className='clickable'>Xóa bộ lọc</div>
        <div>Lưu bộ lọc</div>
      </div>
    </div>
  );
}

export default UsersDropdownFilter;
