import React from 'react';
import { useDispatch } from 'react-redux';
import ButtonIcon from '../../../../../components/button_icon';
import { PAGE_USER_MANAGE_DETAIL } from '../../../pages.consts';
import {
  setCurrentPageForUserManage,
  setCurrentUserId,
} from '../../../userManageSlice';
import { deleteUser, lockUser } from '../../userManageListSlice';

function UsersModalAction({ user }) {
  const dispatch = useDispatch();

  const onClickViewDetailUser = () => {
    dispatch(setCurrentPageForUserManage(PAGE_USER_MANAGE_DETAIL));
    dispatch(setCurrentUserId(user.id));
  };

  const onLockUser = () => {
    dispatch(lockUser(user.id));
  };

  const onDeleteUser = () => {
    dispatch(deleteUser(user.id));
  };

  return (
    <div
      className='dropdown-menu dropdown-menu-right show position-relative'
      x-placement='bottom-end'>
      <ul className='link-list-opt no-bdr'>
        <li>
          <ButtonIcon
            icon={<em className='icon ni ni-eye'></em>}
            label='Xem chi tiết'
            onClick={onClickViewDetailUser}
          />
        </li>
        <li className='divider'></li>
        <li>
          <ButtonIcon
            icon={<em className='icon ni ni-na'></em>}
            label='Khóa tài khoản'
            onClick={onLockUser}
          />
        </li>
        <li>
          <ButtonIcon
            icon={<em className='icon ni ni-trash-alt'></em>}
            label='Xóa tài khoản'
            onClick={onDeleteUser}
          />
        </li>
      </ul>
    </div>
  );
}

export default UsersModalAction;
