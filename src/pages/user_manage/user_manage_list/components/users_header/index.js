import React, { useState } from 'react';
import ButtonIcon from '../../../../../components/button_icon';

function UsersHeader(props) {
  const [mobileShowActionSection, setMobileShowActionSection] = useState(false);

  const onToggleActionSection = () => {
    setMobileShowActionSection((prevState) => !prevState);
  };

  let mobileActionSectionClasses = ['toggle-expand-content'];
  if (mobileShowActionSection) {
    mobileActionSectionClasses.push('expanded d-block');
  }
  return (
    <div className='nk-block-head nk-block-head-sm'>
      <div className='nk-block-between'>
        <div className='nk-block-head-content'>
          <h3 className='nk-block-title page-title'>Danh sách người dùng</h3>
        </div>
        <div className='nk-block-head-content'>
          <div className='toggle-wrap nk-block-tools-toggle'>
            <ButtonIcon
              className='btn btn-icon btn-trigger toggle-expand mr-n1'
              icon={<em className='icon ni ni-menu-alt-r'></em>}
              onClick={onToggleActionSection}
            />
            <div
              className={mobileActionSectionClasses.join(' ')}
              data-content='pageMenu'>
              <ul className='nk-block-tools g-3'>
                <li className='nk-block-tools-opt'>
                  <div className='drodown'>
                    <ButtonIcon
                      className='dropdown-toggle btn btn-icon btn-primary'
                      icon={<em className='icon ni ni-plus'></em>}
                    />
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UsersHeader;
