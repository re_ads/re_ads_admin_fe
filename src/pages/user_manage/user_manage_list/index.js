import React, { useEffect, useState } from 'react';
import ButtonIcon from '../../../components/button_icon';
import ModalDropdown from '../../../components/modal_dropdown';
import Pagination from '../../../components/pagination';
import Select from '../../../components/select';
import Option from '../../../components/select/option';
import UsersTable from './components/users_table';
import DropdownFilter from './components/users_dropdown_filter';
import UsersDropdownSetting from './components/users_dropdown_setting';
import UsersHeader from './components/users_header';
import UsersSearch from './components/users_search';
import { useDispatch, useSelector } from 'react-redux';
import {
  getListUsers,
  selectUserManageListState,
  setPageNumber,
} from './userManageListSlice';

function UsersManageList(props) {
  const [mobileShowFilterSection, setMobileShowFilterSection] = useState(false);
  const [showSearchSection, setShowSearchSection] = useState(false);

  const onCloseMobileFilterSection = () => {
    setMobileShowFilterSection(false);
  };

  const onOpenMobileFilterSection = () => {
    setMobileShowFilterSection(true);
  };

  const onOpenSearchSection = () => {
    setShowSearchSection(true);
  };

  const onCloseSearchSection = () => {
    setShowSearchSection(false);
  };

  let mobileFilterSectionClasses = ['toggle-content'];
  if (mobileShowFilterSection) {
    mobileFilterSectionClasses.push('content-active');
  }

  // handle private logic
  const onRedirectToUserManageDetail = () => {
    props.onChangePage('page_user_manage_detail');
  };

  const { totalPages, page } = useSelector(selectUserManageListState);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getListUsers());
  }, [page]);

  // change page pagination
  const onChangePagePagination = (pageNumber) => {
    dispatch(setPageNumber(pageNumber));
  };

  return (
    <React.Fragment>
      <UsersHeader />
      <div className='nk-block'>
        <div className='card card-bordered card-stretch'>
          <div className='card-inner-group'>
            <div className='card-inner position-relative card-tools-toggle'>
              <div className='card-title-group'>
                <div className='card-tools'>
                  <div className='form-inline flex-nowrap gx-3'>
                    <div className='form-wrap w-150px'>
                      <Select value={2}>
                        <Option value={1}>Option 1</Option>
                        <Option value={2}>Option 2</Option>
                        <Option value={3}>Option 3</Option>
                        <Option value={4}>Option 4</Option>
                      </Select>
                    </div>
                    <div
                      className='btn-wrap'
                      onClick={onRedirectToUserManageDetail}>
                      <span className='d-none d-md-block'>
                        <button className='btn btn-dim btn-outline-light disabled'>
                          Áp dụng
                        </button>
                      </span>
                      <span className='d-md-none'>
                        <button className='btn btn-dim btn-outline-light btn-icon disabled'>
                          <em className='icon ni ni-arrow-right'></em>
                        </button>
                      </span>
                    </div>
                  </div>
                </div>
                <div className='card-tools mr-n1'>
                  <ul className='btn-toolbar gx-1'>
                    <li>
                      <ButtonIcon
                        className='btn btn-icon search-toggle toggle-search'
                        icon={<em className='icon ni ni-search'></em>}
                        onClick={onOpenSearchSection}
                      />
                    </li>
                    <li className='btn-toolbar-sep'></li>
                    <li>
                      <div className='toggle-wrap'>
                        <ButtonIcon
                          className='btn btn-icon btn-trigger toggle'
                          icon={<em className='icon ni ni-menu-right'></em>}
                          onClick={onOpenMobileFilterSection}
                        />
                        <div
                          className={mobileFilterSectionClasses.join(' ')}
                          data-content='cardTools'>
                          <ul className='btn-toolbar gx-1'>
                            <li className='toggle-close'>
                              <ButtonIcon
                                className='btn btn-icon btn-trigger toggle'
                                icon={<em className='icon ni ni-arrow-left' />}
                                onClick={onCloseMobileFilterSection}
                              />
                            </li>
                            <li>
                              <div className='dropdown'>
                                <ModalDropdown
                                  mobileCenter={true}
                                  target={
                                    <ButtonIcon
                                      className='btn btn-trigger btn-icon dropdown-toggle'
                                      icon={
                                        <React.Fragment>
                                          <div className='dot dot-primary'></div>
                                          <em className='icon ni ni-filter-alt'></em>
                                        </React.Fragment>
                                      }
                                    />
                                  }>
                                  <DropdownFilter />
                                </ModalDropdown>
                              </div>
                            </li>
                            <li>
                              <div className='dropdown'>
                                <ModalDropdown
                                  target={
                                    <ButtonIcon
                                      className='btn btn-trigger btn-icon dropdown-toggle'
                                      icon={
                                        <em className='icon ni ni-setting' />
                                      }
                                    />
                                  }>
                                  <UsersDropdownSetting />
                                </ModalDropdown>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <UsersSearch
                active={showSearchSection}
                onClose={onCloseSearchSection}
              />
            </div>
            <div className='card-inner p-0'>
              <UsersTable />
            </div>
            <div className='card-inner'>
              <div className='nk-block-between-md g-3'>
                <Pagination
                  count={totalPages}
                  page={page.number}
                  onChangePage={onChangePagePagination}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default UsersManageList;
