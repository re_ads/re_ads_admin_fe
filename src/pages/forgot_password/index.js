import React from 'react';

function ForgotPassword(props) {
  return (
    <div className='nk-app-root nk-body ui-clean'>
      <div className='nk-split nk-split-page nk-split-md'>
        <div className='nk-split-content nk-block-area nk-block-area-column'>
          <div className='nk-block nk-block-middle nk-auth-body'>
            <div className='brand-logo pb-5'>LOGO HERE</div>
            {/* <div class='nk-block-head'>
              <div class='nk-block-head-content'>
                <h5 class='nk-block-title'>Thank you for submitting form</h5>
                <div class='nk-block-des text-success'>
                  <p>You can now sign in with your new password</p>
                </div>
              </div>
            </div> */}
            <div className='nk-block-head'>
              <div className='nk-block-head-content'>
                <h5 className='nk-block-title'>Đặt lại mật khẩu</h5>
                <div className='nk-block-des'></div>
              </div>
            </div>
            <form action='#'>
              <div className='form-group'>
                <div className='form-label-group'>
                  <label className='form-label' for='default-01'>
                    Email
                  </label>
                </div>
                <input
                  type='text'
                  className='form-control form-control-lg'
                  placeholder='Enter your email address or username'
                />
              </div>
              <div className='form-group'>
                <button className='btn btn-lg btn-primary btn-block'>
                  Gửi yêu cầu
                </button>
              </div>
            </form>
          </div>

          <div className='nk-block nk-auth-footer'>
            <div className='mt-3'>
              <p>© 2020 DTN. All Rights Reserved.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ForgotPassword;
