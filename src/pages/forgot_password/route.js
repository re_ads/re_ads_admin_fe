import { lazy } from 'react';

const route = {
  path: '/forgot',
  exact: true,
  public: true,
  component: lazy(() => import('.')),
};

export default route;
