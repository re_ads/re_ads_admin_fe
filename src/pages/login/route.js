import { lazy } from 'react';

const route = {
  path: '/login',
  exact: true,
  public: true,
  component: lazy(() => import('.')),
};

export default route;
