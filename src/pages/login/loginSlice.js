import { createSlice } from '@reduxjs/toolkit';

export const loginSlice = createSlice({
  name: 'login',
  initialState: {},
  reducers: {},
});

export const {} = loginSlice.actions;

export const selectHomeState = (state) => state.login;

export default loginSlice.reducer;
