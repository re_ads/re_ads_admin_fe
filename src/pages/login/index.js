import React from 'react';

function Login() {
  return (
    <div className='nk-app-root nk-body ui-clean'>
      <div className='nk-split nk-split-page nk-split-md'>
        <div className='nk-split-content nk-block-area nk-block-area-column'>
          <div className='nk-block nk-block-middle nk-auth-body'>
            <div className='brand-logo pb-5'>LOGO HERE</div>
            <div className='nk-block-head'>
              <div className='nk-block-head-content'>
                <h5 className='nk-block-title'>Đăng nhập</h5>
                <div className='nk-block-des'></div>
              </div>
            </div>
            <form action='#'>
              <div className='form-group'>
                <div className='form-label-group'>
                  <label className='form-label' for='default-01'>
                    Email
                  </label>
                </div>
                <input
                  type='text'
                  className='form-control form-control-lg'
                  placeholder='Enter your email address or username'
                />
              </div>
              <div className='form-group'>
                <div className='form-label-group'>
                  <label className='form-label' for='password'>
                    Mật khẩu
                  </label>
                  <div className='link link-primary link-sm' tabIndex='-1'>
                    Quên mật khẩu?
                  </div>
                </div>
                <div className='form-control-wrap'>
                  <div
                    tabIndex='-1'
                    className='form-icon form-icon-right passcode-switch'
                    data-target='password'>
                    <em className='passcode-icon icon-show icon ni ni-eye'></em>
                    <em className='passcode-icon icon-hide icon ni ni-eye-off'></em>
                  </div>
                  <input
                    type='password'
                    className='form-control form-control-lg'
                    id='password'
                    placeholder='Enter your passcode'
                  />
                </div>
              </div>
              <div className='form-group'>
                <button className='btn btn-lg btn-primary btn-block'>
                  Đăng nhập
                </button>
              </div>
            </form>
          </div>
          <div className='nk-block nk-auth-footer'>
            <div className='mt-3'>
              <p>© 2020 DTN. All Rights Reserved.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
