import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

function ErrorPage(props) {
  const { t } = useTranslation();
  return (
    <div className='nk-app-root nk-body ui-clean'>
      <div className='nk-main'>
        <div className='nk-wrap justify-center'>
          <div className='nk-content'>
            <div className='nk-block nk-block-middle wide-xs mx-auto'>
              <div className='nk-block-content nk-error-ld text-center'>
                <h1 className='nk-error-head'>404</h1>
                <h3 className='nk-error-title'>Oops! Tại sao bạn ở đây?</h3>
                <p className='nk-error-text'>{t('error_404')}</p>
                <Link to='#' className='btn btn-lg btn-primary mt-2'>
                  Quay lại trang chủ
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ErrorPage;
