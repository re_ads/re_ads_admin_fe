import { request } from './request';

export const categoryService = {
  getCategoryList() {
    return request({
      url: '/categories',
    });
  },
};
