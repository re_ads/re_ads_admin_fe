import { requestUtils } from '../../utils/requestUtils';
import { request } from './request';

export const userManageService = {
  getListUsers(page = { number: 1, size: 10 }, filters = {}, sort = {}) {
    const pageParams = requestUtils.createParamsRequest('page', page);
    const filterParams = requestUtils.createParamsRequest('filter', filters);
    const sortParams = requestUtils.createParamsRequest('sort', sort);

    return request({
      url: '/users?' + pageParams + filterParams + sortParams,
      method: 'GET',
    });
  },

  getUserDetail(userId) {
    return request({
      url: '/users/' + userId,
      method: 'GET',
    });
  },

  updateUserStatus(userId, status) {
    return request({
      url: '/users/' + userId + '/status',
      method: 'PATCH',
      data: {
        status,
      },
    });
  },
  getRoles() {
    return request({
      url: '/roles',
      method: 'GET',
    });
  },
};
