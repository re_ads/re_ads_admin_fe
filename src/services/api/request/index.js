import axios from 'axios';
import { BASE_URL } from '../../../configs';

const instance = axios.create({
  baseURL: BASE_URL,
});

instance.defaults.headers.common['Authorization'] = 'token';
instance.defaults.timeout = 2500;

const overlay = document.createElement('div');
overlay.className = 'overlay-request';

// Add a request interceptor
instance.interceptors.request.use(
  function (config) {
    // Do something before request is sent

    document.body.appendChild(overlay);
    return config;
  },
  function (error) {
    // Do something with request error
    document.body.removeChild(overlay);

    return Promise.reject(error);
  }
);

// Add a response interceptor
instance.interceptors.response.use(
  function (response) {
    document.body.removeChild(overlay);
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    document.body.removeChild(overlay);
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);

export const request = ({ method, url, data }) => {
  return new Promise((resolve, reject) => {
    instance({ method, url, data }, { baseURL: BASE_URL })
      .then((response) => {
        resolve(response.data);
      })
      .catch((err) => {
        if (err.response) {
          console.log(err.response.data);
          console.log(err.response.status);
          console.log(err.response.headers);
        }
        reject(reject);
      });
  });
};
