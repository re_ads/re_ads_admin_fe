import { userManageService } from './userManageService';
import { categoryService } from './categoryService';
import { postService } from './postService';
export const services = {
  userManageService,
  categoryService,
  postService,
};
