import { request } from './request';

export const postService = {
  createPost({
    title = '',
    metaTitle = '',
    summary = '',
    content = '',
    mapLatitude = null,
    mapLongitude = null,
    tags = [],
    categoryId = '',
    thumbnail = null,
  }) {
    const formData = new FormData();
    formData.append('title', title);
    formData.append('metaTitle', metaTitle);
    formData.append('summary', summary);
    formData.append('content', content);
    formData.append('mapLatitude', mapLatitude);
    formData.append('mapLongitude', mapLongitude);
    formData.append('tags', JSON.stringify(tags));
    formData.append('categoryId', categoryId);
    formData.append('thumbnail', thumbnail);

    return request({
      url: '/posts',
      method: 'POST',
      data: formData,
    });
  },
};
