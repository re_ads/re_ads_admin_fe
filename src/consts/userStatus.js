export const userStatus = {
  ACTIVE: 1,
  UNACTIVE: 0,
  DELETED: 3,
  ACTIVE_NAME: 'active',
  UNACTIVE_NAME: 'unactive',
  DELETED_NAME: 'deleted',
};
