import React from 'react';
import Header from '../../components/header';
import SideBar from '../../components/sidebar';

function MainSite({ component }) {
  return (
    <div className='nk-body npc-crypto has-sidebar has-sidebar-fat ui-clean'>
      <div className='nk-app-root'>
        <div className='nk-main'>
          <SideBar />
          <div className='nk-wrap'>
            <Header />
            <div className='nk-content'>
              <div className='container-fluid'>
                <div className='nk-content-inner'>
                  <div className='nk-content-body'>{component}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainSite;
