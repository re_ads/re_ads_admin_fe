import React, { lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import MainSite from './main_site';

import routes from '../pages/routes';
import Loading from '../components/loading';

const ErrorPage = lazy(() => import('../pages/error'));

function App() {
  return (
    <Router>
      <Switch>
        {routes.map(({ component: Component, path, ...rest }) => {
          return rest.public ? (
            <Route
              render={(props) => (
                <React.Suspense fallback={<Loading />}>
                  <Component {...props} />
                </React.Suspense>
              )}
              key={path}
              path={path}
              {...rest}
            />
          ) : (
            <Route
              render={(props) => (
                <React.Suspense fallback={<Loading />}>
                  <MainSite component={<Component {...props} />} />
                </React.Suspense>
              )}
              key={path}
              path={path}
              {...rest}
            />
          );
        })}
        <Route
          render={(props) => (
            <React.Suspense fallback={<Loading />}>
              <ErrorPage {...props} />
            </React.Suspense>
          )}
          path='*'
        />
      </Switch>
    </Router>
  );
}

export default App;
