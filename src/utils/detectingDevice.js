const MAX_WIDTH_MOBILE = 576;

export const detectingDevice = {
    isMobile() {
        return document.body.offsetWidth < MAX_WIDTH_MOBILE
    },
}