export const requestUtils = {
  createParamsRequest(prefix, object = {}) {
    let params = [];
    const keys = Object.keys(object);
    for (const key of keys) {
      const param = `${prefix}[${key}]=${object[key]}`;
      params.push(param);
    }

    if (Array.isArray(params) && params.length) {
      return '&' + params.join('&');
    }

    return '';
  },
};
